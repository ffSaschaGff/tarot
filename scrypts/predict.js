document.addEventListener("DOMContentLoaded", function() {
    loadCards();
})

function predict() {
    result = randomCards(3);
    cardsRoot = document.getElementById("cards-container");
    cardsRoot.innerHTML = "";
    desc = "";
    for (cardIndex in result) {
        oneCard = result[cardIndex];
        down = 0.5 > Math.random();

        cardElement = document.createElement("img");
        cardElement.src = "imgs/back.jpg";
        cardElement.alt = oneCard.name;
        if (down) {
            cardElement.classList.add("img-upside-down");
        }
        cardsRoot.append(cardElement);
        flipCard(oneCard, cardElement);
        switch (cardIndex) {
            case "0":
                desc = desc + "Прошлое: ";
                break;
            case "1":
                desc = desc + "Настоящее: ";
                break;
            case "2":
                desc = desc + "Будущее: ";
                break;
        }
        cardDesc = down ? oneCard.down_desc : oneCard.up_desc
        desc = desc + cardDesc.join(", ") + "</br> ";
    }

    document.getElementById("cards-description").innerHTML = desc;
}

function flipCard(card, cardElement) {
    setTimeout(function() {
        cardElement.classList.add("card_back");
        setTimeout(function() {
            cardElement.src = "imgs/" + card.name + ".jpg";
            cardElement.classList.add("card_front");
        }, 1000);
    }, 10);
}
