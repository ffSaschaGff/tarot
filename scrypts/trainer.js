var app;

document.addEventListener("DOMContentLoaded", function() {
    loadCards(initTrain);
})

function initTrain() {
    app = new Vue({
        el: '#card-trainer',
        data: {
            choosenCard: {
                name: "back"
            },
            selectedCard: "",
            variants: [],
            results: {
                correct: 0,
                wrong: 0,
                skipped: 0
            },
            straitOnly: false
        },
        watch: {
            straitOnly: function(val) {
                app.results = {
                    correct: 0,
                    wrong: 0,
                    skipped: 0
                };
                nextCardForTrain();
            }
        },
        methods: {
            selectVariant: function(event) {
                event.preventDefault();
                if (app.selectedCard) {
                    nextCardForTrain();
                    return;
                }

                app.selectedCard = event.target.value;
                app.results.correct++;
                for (variant of app.variants) {
                    if (app.choosenCard == variant) {
                        variant.mark = "correct";
                    } else if (app.selectedCard == variant.name) {
                        variant.mark = "wrong"; 
                        app.results.wrong++;
                        app.results.correct--;        
                    }
                }
            },
            nextCard: function() {
                if (!app.selectedCard) {
                    app.results.skipped++;
                }
                nextCardForTrain();
            }
        }
    });

    nextCardForTrain();
}

function nextCardForTrain() {
    app.selectedCard = "";
    variants = randomCards(4);
    for (variant of variants) {
        if (!app.straitOnly) {
            variant.down = down = 0.5 > Math.random();
        }
        variant.mark = "";
    }
    app.variants = variants;
    app.choosenCard = variants[Math.floor(Math.random()*variants.length)];
}
