var app

document.addEventListener("DOMContentLoaded", function() {
    loadCards(fillTable);
})

function fillTable() {
    for (card of cards) {
        card.is_visible = true
    }

    app = new Vue({
        el: '#card-list',
        data: {
           cards: cards,
           search: "",
        },
        watch: {
            search: function (val) {
                var visibleFunc;
                if (val == "") {
                    visibleFunc = function(cardName) {
                        return true;
                    }   
                } else {
                    searchString = normalizedSearch(val);
                    visibleFunc = function(cardName) {
                        return cardName.lastIndexOf(searchString) != -1;
                    }
                }

                for (card of this.cards) {
                    card.is_visible = visibleFunc(card.name.toLowerCase());
                }
            } 
        }
    })
}

function normalizedSearch(input) {
    replacers = [
        ['2', 'двойка'],
        ['3', 'тройка'],
        ['4', 'четверка'],
        ['5', 'пятерка'],
        ['6', 'шестерка'],
        ['7', 'семерка'],
        ['8', 'восьмерка'],
        ['9', 'девятка'],
        ['10', 'десятка'],
    ];

    for (replacer of replacers) {
        input = input.replaceAll(replacer[0],replacer[1]);
    }

    return input.toLowerCase();
}
