let cards = []

function loadCards(callback) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/cards.json');
    xhr.responseType = 'json';
    xhr.send();
    xhr.onload = function() {
        if (xhr.status == 200) {
            cards =  xhr.response;
            if (callback) {
                callback();
            }
        }
    };
}

function randomCards(count) {
    return [...cards].sort(() => 0.5 - Math.random()).slice(0, count)
}
